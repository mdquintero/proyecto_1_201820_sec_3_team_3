package api;


import java.util.Date;

import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Bicicleta;
import model.vo.Station;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager.
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	void loadTrips(String tripsFile, int number);
	
	
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 */
	void loadStations(String stationsFile);
	
	/**
	 * Method to load Divy BikeRoutes (JSON)
	 * @return if it was successful
	 */
	boolean cargarSistema(String direccionJson);

	/**
	 * Se crea un queue con los viajes que empiecen y terminen dentro de las fechas dadas por parametro 
	 * @param inicio fecha de inicio del periodo
	 * @param fin fecha del final del periodo
	 * @return una cola con los viajes que inician y finalizan en un periodo.
	 */
	Queue<VOTrip> viajesPorTiempo(Date inicio, Date fin);
	
	/**
	 * Ordena un queue por cantidad de viajes
	 * @param inicio fecha de inicio del periodo
	 * @param fin fecha del final del periodo
	 * @return una cola ordenada de mayor a menor en cantidad de viajes
	 */
	Queue<VOTrip> ordenarPorCantidadDeViajes(Date inicio, Date fin);
	
	
	/**
	 * 
	 * @param ID identificador de la bicicleta
	 * @param inicio fecha de inicio del periodo
	 * @param fin fecha del final del periodo
	 * @return una cola con la información de los viajes de la bicicleta identificada por el id en el periodo de tiempo
	 */
	Queue<VOTrip> viajesDeBicicleta(int ID, Date inicio, Date fin);
	
	Queue<VOTrip> viajesTerminadosEn(int ID, Date inicio, Date fin);
	
	Queue<Station> estacionesDespuesDe(Date fecha);
	
	Queue<Bicicleta> ordenarPorDistancia(Date inicio, Date fin);
	
	Queue<VOTrip> viajesDeBicicleta(int ID, Double duracion, String sexo);
	
	Queue<VOTrip> viajesDeEstacion(int ID,Date inicio, Date fin);
	
	Queue<VOTrip> validarViajes(int ID, Date inicio, Date fin);
	
	Queue<Bicicleta> bicicletasMasUsadas(int cantidadAOrdenar);
	
}
