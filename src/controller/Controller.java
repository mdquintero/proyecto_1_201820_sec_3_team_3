package controller;

import java.time.LocalDateTime;

import API.IManager;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;


public class Controller {
    private static IManager manager = new Manager();
    public static IManager getManager()
    {
    	return manager;
    }
    public static Queue<Trip> A1(LocalDateTime fechaInicial, LocalDateTime fechaFinal){
        return manager.A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
    }

    public static LinkedList<Bike> A2(LocalDateTime fechaInicial, LocalDateTime fechaFinal){
        return manager.A2BicicletasOrdenadasPorNumeroViajes(fechaInicial, fechaFinal);
    }

    public LinkedList<Trip> A3(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return manager.A3ViajesPorBicicletaPeriodoTiempo(bikeId, fechaInicial, fechaFinal);
    }

    public LinkedList<Trip> A4(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return manager.A4ViajesPorEstacionFinal(endStationId, fechaInicial, fechaFinal);
    }

    public Queue<Station> B1(LocalDateTime fechaComienzo) {
        return manager.B1EstacionesPorFechaInicioOperacion(fechaComienzo);
    }

    public LinkedList<Bike> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return manager.B2BicicletasOrdenadasPorDistancia(fechaInicial, fechaFinal);
    }

    public LinkedList<Trip> B3(int bikeId, int tiempoMaximo, String genero) {
        return manager.B3ViajesPorBicicletaDuracion(bikeId, tiempoMaximo, genero);
    }

    public LinkedList<Trip> B4(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return manager.B4ViajesPorEstacionInicial(startStationId, fechaInicial, fechaFinal);
    }
    public static void C1cargar(String dataTrips, String dataStations){
    	manager.C1cargar(dataTrips, dataStations);
    }
    public Queue<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal){
    	return manager.C2ViajesValidadosBicicleta(bikeId, fechaInicial, fechaFinal);
    }
    public  LinkedList<Bike> C3BicicletasMasUsadas(int topBicicletas){
		return manager.C3BicicletasMasUsadas(topBicicletas);
    }
    public LinkedList<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal){
    	return manager.C4ViajesEstacion(StationId, fechaInicial, fechaFinal);
    }
}
