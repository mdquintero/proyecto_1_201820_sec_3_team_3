package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Calendar;
import java.util.Date;

import com.google.gson.*;
import api.IDivvyTripsManager;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Bicicleta;
import model.vo.Route;
import model.vo.Station;
import model.vo.VOTrip;

public class DivvyTripsManager implements IDivvyTripsManager {

	private Stack<VOTrip> stackTrips= new Stack<VOTrip>();

	private Queue<VOTrip> queueTrips = new Queue<VOTrip>();

	private LinkedList<Station> listaEncadenadaStations= new LinkedList<Station>();

	private Queue<Route> queueRoute = new Queue<Route>( );


	public void loadStations (String stationsFile) {
		try{
			FileReader fr = new FileReader(new File(stationsFile));
			BufferedReader br = new BufferedReader(fr);

			String line = br.readLine();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readLine();

			while(line!=null)
			{


				String[] lineArray = line.split(",");

				Station s = new Station(lineArray[0], lineArray[1], lineArray[2], Double.parseDouble(lineArray[3].replace("\"","")), Double.parseDouble(lineArray[4].replace("\"","")), 
						Integer.parseInt(lineArray[5].replace("\"","")), Calendar.getInstance());


				listaEncadenadaStations.add(s);

				line = br.readLine();
			}

			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void loadTrips (String tripsFile, int number) {



		try{
			FileReader fr = new FileReader(new File(tripsFile));
			BufferedReader br = new BufferedReader(fr);

			VOTrip t=null;


			String line = br.readLine();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readLine();


			if(number==4) {
				while(line!=null)
				{

					//System.out.println(line);
					String[] lineArray = line.split(",");

					if(lineArray.length==12){
						t = new VOTrip(Integer.parseInt(lineArray[0]),  lineArray[1],  lineArray[2], Integer.parseInt(lineArray[3]), 
								Integer.parseInt(lineArray[4]), Integer.parseInt(lineArray[5]), lineArray[6], Integer.parseInt(lineArray[7]), lineArray[8], lineArray[9],
								lineArray[10], Integer.parseInt(lineArray[11]));
					}
					else{
						t = new VOTrip(Integer.parseInt(lineArray[0]),  lineArray[1],  lineArray[2], Integer.parseInt(lineArray[3]), 
								Integer.parseInt(lineArray[4]), Integer.parseInt(lineArray[5]), lineArray[6], Integer.parseInt(lineArray[7]), lineArray[8], lineArray[9]);
					}
					stackTrips.push(t);
					queueTrips.enqueue(t);
					line = br.readLine();

				}

				br.close();

			}
			else {
				while(line!=null)
				{
					String[] lineArray = line.split(",");

					if(lineArray[10].equals("\"\"")){

						t = new VOTrip(Integer.parseInt(lineArray[0].replace("\"","")), lineArray[1], lineArray[2], Integer.parseInt(lineArray[3].replace("\"","")), 
								Integer.parseInt(lineArray[4].replace("\"","")), Integer.parseInt(lineArray[5].replace("\"","")), lineArray[6], Integer.parseInt(lineArray[7].replace("\"","")), lineArray[8], lineArray[9]);

					}
					else{
						t = new VOTrip(Integer.parseInt((lineArray[0].replace("\"",""))), lineArray[1], lineArray[2], Integer.parseInt(lineArray[3].replace("\"","")), 
								Integer.parseInt(lineArray[4].replace("\"","")), Integer.parseInt(lineArray[5].replace("\"","")), lineArray[6], Integer.parseInt(lineArray[7].replace("\"","")), lineArray[8], lineArray[9],
								lineArray[10], Integer.parseInt(lineArray[11].replace("\"","")));
					}
					stackTrips.push(t);
					queueTrips.enqueue(t);
					line = br.readLine();
				}
				br.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

		
	public boolean cargarSistema(String direccionJson) 
	{
		try
		{
			JsonParser parser = new JsonParser();
			JsonObject inicio = (JsonObject) parser.parse(new FileReader(direccionJson));
			JsonArray fin =null;
			if(inicio!=null)
			{
				fin = inicio.getAsJsonArray("data");
			}

			for (int i = 0; fin != null && i < fin.size(); i++)

			{

				JsonArray obj = (JsonArray)fin.get(i);
				String tipo ="";
				String ruta = "";
				String calleReferencia ="";
				double distancia = 0.0;
				String calleLimiteExtremo1 = "";
				String calleLimiteExtremo2 ="";				
				
				if(obj.get(8)!=null)
				tipo = obj.get(8).getAsString();

				if(obj.get(9)!=null)
				ruta = obj.get(9).getAsString();

				if(obj.get(11)!=null)
				calleReferencia = obj.get(11).getAsString();

				if(obj.get(15)!=null)
				distancia = obj.get(15).getAsDouble();

				if(obj.get(12)!=null)
				calleLimiteExtremo1 = obj.get(12).getAsString();

				if(obj.get(13)!=null)
				calleLimiteExtremo2 = obj.get(13).getAsString();
				
				Route t = new Route(tipo, ruta, calleReferencia, calleLimiteExtremo1, calleLimiteExtremo2, distancia);
				
				queueRoute.enqueue(t);
				System.out.println("Se ha a�adido la ruta numero " + (i+1));

				
			}
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}

		System.out.println("Inside loadServices with " + direccionJson);
		return false;
	}

	@Override
	public Queue<VOTrip> viajesPorTiempo(Date inicio, Date fin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<VOTrip> ordenarPorCantidadDeViajes(Date inicio, Date fin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<VOTrip> viajesDeBicicleta(int ID, Date inicio, Date fin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<VOTrip> viajesTerminadosEn(int ID, Date inicio, Date fin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<Station> estacionesDespuesDe(Date fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<Bicicleta> ordenarPorDistancia(Date inicio, Date fin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<VOTrip> viajesDeBicicleta(int ID, Double duracion, String sexo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<VOTrip> viajesDeEstacion(int ID, Date inicio, Date fin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<VOTrip> validarViajes(int ID, Date inicio, Date fin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<Bicicleta> bicicletasMasUsadas(int cantidadAOrdenar) {
		// TODO Auto-generated method stub
		return null;
	}
	 



}
