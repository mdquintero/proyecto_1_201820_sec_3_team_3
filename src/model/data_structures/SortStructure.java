package model.data_structures;


import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

public class SortStructure {
	public static Queue<Trip> mergeSortTripQueue(Queue<Trip> comparable) {
		int tamarreglo = comparable.size()/2;
		Trip[] a = new Trip [tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.dequeue();
				
			}
			MergeSort<Trip> mergue = new MergeSort<Trip>(a);
			a = mergue.getSortedItems();
		}
		Queue<Trip> r = new Queue<Trip>();
		for(int i=0; i<a.length;i++) {
			r.enqueue(a[i]);
		}
		return r ;
	}
	
	
	
	public static  LinkedList<Trip> mergeSortTripList(LinkedList<Trip> comparable) {
		int tamarreglo = comparable.size()/2;
		Trip[] a = new Trip [tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.get(i).getItem();
			}
			MergeSort<Trip> mergue = new MergeSort<Trip>(a);
			a = mergue.getSortedItems();
		}
		LinkedList<Trip> r = new LinkedList<Trip>();
		for(int i=0; i<a.length;i++) {
			r.add(a[i]);
		}
		return r ;
	}
	
	public static Stack<Trip> mergeSortTripStack(Stack<Trip> comparable) {
		int tamarreglo = comparable.size()/2;
		Trip[] a = new Trip[tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.pop();
			}
			MergeSort<Trip> mergue = new MergeSort<Trip>(a);
			a = mergue.getSortedItems();
		}
		Stack<Trip> r = new Stack<Trip>();
		for(int i=0; i<a.length;i++) {
			r.push(a[i]);;
		}
		return r;
	}
	
	public static Stack<Trip> mergeSortTripStackById(Stack<Trip> comparable) {
		int tamarreglo = comparable.size()/2;
		Trip[] a = new Trip[tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.pop();
			}
			MergeTrips mergue = new MergeTrips(a);
			a = mergue.getSortedItems(1);
		}
		Stack<Trip> r = new Stack<Trip>();
		for(int i=0; i<a.length;i++) {
			r.push(a[i]);;
		}
		return r;
	}
	
	public static Queue<Station> mergeSortStationQueue(Queue<Station> comparable) {
		int tamarreglo = comparable.size()/2;
		Station[] a = new Station [tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.dequeue();
			}
			MergeSort<Station> mergue = new MergeSort<Station>(a);
			a = mergue.getSortedItems();
		}
		Queue<Station> r = new Queue<Station>();
		for(int i=0; i<a.length;i++) {
			r.enqueue(a[i]);
		}
		return r ;
	}
	
	
	
	public static LinkedList<Station> mergeSortStationList(LinkedList<Station> comparable) {
		int tamarreglo = comparable.size()/2;
		Station[] a = new Station [tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.get(i).getItem();
			}
			MergeSort<Station> mergue = new MergeSort<Station>(a);
			a = mergue.getSortedItems();
		}
		LinkedList<Station> r = new LinkedList<Station>();
		for(int i=0; i<a.length;i++) {
			r.add(a[i]);
		}
		return r ;
	}
	
	public static Stack<Station> mergeSortStationStack(Stack<Station> comparable) {
		int tamarreglo = comparable.size()/2;
		Station[] a = new Station[tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.pop();
			}
			MergeSort<Station> mergue = new MergeSort<Station>(a);
			a = mergue.getSortedItems();
		}
		Stack<Station> r = new Stack<Station>();
		for(int i=0; i<a.length;i++) {
			r.push(a[i]);;
		}
		return r ;
	}
	
	
	
	
	public static  LinkedList<Bike> mergeSortBikes(LinkedList<Bike> comparable) {
		int tamarreglo = comparable.size()/2;
		Bike[] a = new Bike [tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.get(i).getItem();
			}
			MergeBikes mergue = new MergeBikes(a);
			a = mergue.getSortedItems(0);
		}
		LinkedList<Bike> r = new LinkedList<Bike>();
		for(int i=0; i<a.length;i++) {
			r.add(a[i]);
		}
		return r;
	}
	
	public static  LinkedList<Bike> mergeSortBikesByDurations(LinkedList<Bike> comparable, int numBikes) {
		int tamarreglo = comparable.size()/2;
		Bike[] a = new Bike [tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.get(i).getItem();
			}
			MergeBikes mergue = new MergeBikes(a);
			a = mergue.getSortedItems(2);
		}
		LinkedList<Bike> r = new LinkedList<Bike>();
		for(int i=0; i<numBikes;i++) {
			r.add(a[i]);
		}
		return r;
	}
	
	public static  LinkedList<Bike> mergeSortBikesByDistances(LinkedList<Bike> comparable) {
		int tamarreglo = comparable.size()/2;
		Bike[] a = new Bike [tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.get(i).getItem();
			}
			MergeBikes mergue = new MergeBikes(a);
			a = mergue.getSortedItems(1);
		}
		LinkedList<Bike> r = new LinkedList<Bike>();
		for(int i=0; i<a.length;i++) {
			r.add(a[i]);
		}
		return r;
	}



	public static Queue<Trip> mergeSortTripQueueById(Queue<Trip> comparable) {
		int tamarreglo = comparable.size()/2;
		Trip[] a = new Trip [tamarreglo];
		if(comparable!=null)
		{
			for(int i=0; i<tamarreglo;i++) {
				a[i]= comparable.dequeue();
				System.out.println(a[i].getBikeId());
			}
			MergeTrips mergue = new MergeTrips(a);
			a = mergue.getSortedItems(1);
		}
		Queue<Trip> r = new Queue<Trip>();
		for(int i=0; i<a.length;i++) {
			r.enqueue(a[i]);
		}
		return r;
	}
	}
	
	

