package model.vo;

public class VOBike {

	private int bikeId;
	
	public VOBike()
	{
		setBikeId(0);
	}

	public int getBikeId() {
		return bikeId;
	}

	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}
}
