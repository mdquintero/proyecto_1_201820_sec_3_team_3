package model.vo;

import java.util.Calendar;

/**
 * Representation of a Trip object.
 */
public class VOTrip {

	//ATRIBUTOS
	
		private int id;
		
		private String strTime;
		
		private String endTime;
		
		private int bikeId;
		
		private int tripDuration;
		
		private int idFromStation;
		
		private String nameFromStation;
		
		private int idToStation;
		
		private String nameToStation;
		
		private String userType;
		
		private String gender;
		
		private int birthday;
		
		//CONSTRUCTORES
		
		public VOTrip( int pId, String pStrTime, String pEndTime, int pBikeId, int pTripDuration, 
				int pIdFromStation, String pNameFromStation, int pIdToStation, String pNameToStation,
				String pUserType, String pGender, int pBirthday) {
			
			this.setId(pId);
			this.setStrTime(pStrTime);
			this.setEndTime(pEndTime);
			this.setBikeId(pBikeId);
			this.setTripDuration(pTripDuration);
			this.setIdFromStation(pIdFromStation);
			this.setNameFromStation(pNameFromStation);
			this.setIdToStation(pIdToStation);
			this.setNameToStation(pNameToStation);
			this.setUserType(pUserType);
			this.setGender(pGender);
			this.setBirthday(pBirthday);
		}
		
		public VOTrip( int pId, String pStrTime, String pEndTime, int pBikeId, int pTripDuration, 
				int pIdFromStation, String pNameFromStation, int pIdToStation, String pNameToStation,
				String pUserType) {
			
			this.setId(pId);
			this.setStrTime(pStrTime);
			this.setEndTime(pEndTime);
			this.setBikeId(pBikeId);
			this.setTripDuration(pTripDuration);
			this.setIdFromStation(pIdFromStation);
			this.setNameFromStation(pNameFromStation);
			this.setIdToStation(pIdToStation);
			this.setNameToStation(pNameToStation);
			this.setUserType(pUserType);

		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getStrTime() {
			return strTime;
		}

		public void setStrTime(String strTime) {
			this.strTime = strTime;
		}

		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}

		public int getBikeId() {
			return bikeId;
		}

		public void setBikeId(int bikeId) {
			this.bikeId = bikeId;
		}

		public int getTripDuration() {
			return tripDuration;
		}

		public void setTripDuration(int tripDuration) {
			this.tripDuration = tripDuration;
		}

		public int getIdFromStation() {
			return idFromStation;
		}

		public void setIdFromStation(int idFromStation) {
			this.idFromStation = idFromStation;
		}

		public String getNameFromStation() {
			return nameFromStation;
		}

		public void setNameFromStation(String nameFromStation) {
			this.nameFromStation = nameFromStation;
		}

		public int getIdToStation() {
			return idToStation;
		}

		public void setIdToStation(int idToStation) {
			this.idToStation = idToStation;
		}

		public String getNameToStation() {
			return nameToStation;
		}

		public void setNameToStation(String nameToStation) {
			this.nameToStation = nameToStation;
		}

		public String getUserType() {
			return userType;
		}

		public void setUserType(String userType) {
			this.userType = userType;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public int getBirthday() {
			return birthday;
		}

		public void setBirthday(int birthday) {
			this.birthday = birthday;
		}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return getId();
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		return getTripDuration();
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		
		return getNameFromStation();
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		
		return getNameToStation();
	}
}
