package model.vo;

public class Route {


	public String tipo; 

	public String ruta;

	public String calleReferencia;

	public String calleLimiteExtremo1;

	public String calleLimiteExtremo2;

	public double longitud;

	public Route(String pTipo, String pRuta, String pCalleReferencia, String pCalleLimiteExtremo1, String pCalleLimiteExtremo2, double pLongitud)
	{
		tipo = pTipo;
		ruta = pRuta;
		calleReferencia = pCalleReferencia;
		calleLimiteExtremo1 = pCalleLimiteExtremo1;
		calleLimiteExtremo2 = pCalleLimiteExtremo2;
		longitud = pLongitud;
	}

	public String getTipo()
	{
		return tipo;
	}

	public void setTipo(String tipo) 
	{
		this.tipo = tipo;
	}

	public String getRuta()
	{
		return ruta;
	}

	public void setRuta(String ruta) 
	{
		this.ruta = ruta;
	}

	public String getCalleReferencia()
	{
		return calleReferencia;
	}

	public void setCalleReferencia(String calleReferencia) 
	{
		this.calleReferencia = calleReferencia;
	}

	public String getCalleLimiteExtremo1() 
	{
		return calleLimiteExtremo1;
	}

	public void setCalleLimiteExtremo1(String calleLimiteExtremo1) 
	{
		this.calleLimiteExtremo1 = calleLimiteExtremo1;
	}

	public String getCalleLimiteExtremo2()
	{
		return calleLimiteExtremo2;
	}

	public void setCalleLimiteExtremo2(String calleLimiteExtremo2)
	{
		this.calleLimiteExtremo2 = calleLimiteExtremo2;
	}

	public double getLongitud()
	{
		return longitud;
	}

	public void setLongitud(double longitud)
	{
		this.longitud = longitud;
	}










}
