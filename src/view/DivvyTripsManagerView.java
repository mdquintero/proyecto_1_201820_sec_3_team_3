package view;

import java.util.Scanner;

import controller.Controller;

import model.data_structures.LinkedList;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					//System.out.println("Escoja el numero del archivo que quiere cargar (entre 1 y 2)");
					//int num = sc.nextInt();
					Controller.loadStations();
					break;
					
				case 2:
					//System.out.println("Escoja el numero del archivo que quiere cargar (entre 1 y 4)");
					//int numb = sc.nextInt();
					Controller.loadTrips();
					break;
					
				case 3:
					Controller.cargarSistema();
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cree una nueva coleccion de estaciones");
		System.out.println("2. Cree una nueva coleccion de viajes");
		System.out.println("3. Cree una nueva coleccion de ciclorutas");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
