package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.LinkedList;

public class testDoubleLinkedList {


	//ATRIBUTOS
	private LinkedList<Integer> listaIntegers;


	private LinkedList<String> listaStrings;


	//METODOS
	private void setupEscenarioIntegers(){

		listaIntegers = new LinkedList<Integer>();

		listaIntegers.add(Integer.valueOf(1));
		listaIntegers.add(Integer.valueOf(2));
		listaIntegers.add(Integer.valueOf(3));
		listaIntegers.add(Integer.valueOf(4));
		listaIntegers.add(Integer.valueOf(5));
		listaIntegers.add(Integer.valueOf(6));
		listaIntegers.add(Integer.valueOf(7));

	}

	private void setupEscenarioStrings(){

		listaStrings = new LinkedList<String>();

		listaStrings.add("aaa");
		listaStrings.add("bbb");
		listaStrings.add("ccc");
		listaStrings.add("ddd");
		listaStrings.add("fff");
		listaStrings.add("ggg");
		listaStrings.add("hhh");

	}

	
	@Test
	public void testAdd(){
		setupEscenarioIntegers();
		setupEscenarioStrings();


		assertEquals( "El metodo add no agrego todos los elementos a la lista", 7, listaIntegers.size() );
		assertEquals( "El metodo add no agrego todos los elementos a la lista", 7, listaStrings.size() );

	}

	@Test
	public void testAddFirst(){
		listaIntegers = new LinkedList<Integer>();
		listaStrings = new LinkedList<String>();


		listaIntegers.addFirst(1);
		listaStrings.addFirst("aaa");
		assertTrue( "La lista no deberia estar vacia", !listaIntegers.isEmpty() );
		assertTrue( "La lista no deberia estar vacia", !listaStrings.isEmpty() );

	}

	@Test
	public void testRemove(){
		setupEscenarioIntegers();
		setupEscenarioStrings();


		listaStrings.remove(2);
		listaIntegers.remove(2);

		assertEquals( "El metodo remove no elimino el elemento de la lista", 6, listaIntegers.size() );
		assertEquals( "El metodo remove no elimino el elemento de la lista", 6, listaStrings.size() );
	}

	@Test
	public void testIsEmpty(){
		listaIntegers = new LinkedList<Integer>();
		listaStrings = new LinkedList<String>();

		assertTrue( "La lista deberia estar vacia", listaIntegers.isEmpty() );
		assertTrue( "La lista deberia estar vacia", listaStrings.isEmpty() );
	}

	@Test
	public void testGetFromPos(){
		
		
		setupEscenarioIntegers();
		
		setupEscenarioStrings();
		
		assertTrue( "El metodo get no retorno el elemento esperado", listaStrings.get(5).getItem().equals("bbb") );
		assertTrue( "El metodo get no retorno el elemento esperado", (listaIntegers.get(1).getItem().intValue())==1 );
	}
	
	
}
